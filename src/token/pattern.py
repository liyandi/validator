# -*- coding: utf-8 -*-
#!/usr/bin/python                        
##################################################
# AUTHOR : Yandi LI
# CREATED_AT : 2016-03-19
# LAST_MODIFIED : 2016-03-20 23:27:33
# USAGE : python -m token.pattern <FILEIN> <FILEOUT>
# PURPOSE : TODO
##################################################
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
LOG_DIR = os.path.join(BASE_DIR, 'log')
SRC_DIR = os.path.join(BASE_DIR, 'src')
DATA_DIR = os.path.join(BASE_DIR, 'data')
os.sys.path.insert(0, SRC_DIR) 

import subprocess32 as subprocess # pip install subprocess
from misc import mylogging 
from .encode import Encoder
from .sorting import Sorted
import jieba

logging = mylogging.getLogger('Pattern', LOG_DIR)


class Pattern(object):
  
  JAR_PATH = os.path.realpath(os.path.join(BASE_DIR, 'lib', 'spmf.jar'))
  TMP_IN = os.path.join(DATA_DIR, 'tmp', 'tmp_in')
  TMP_OUT = os.path.join(DATA_DIR, 'tmp', 'tmp_out')

  @staticmethod
  def _group(lines, by=1000):
    """ 将行按照给定数目分批次输出"""
    group = []
    for cnt, line in enumerate(lines):
      if (cnt+1) % by != 0:
        group.append(line)
      else:
        yield group
        group = []


  @classmethod
  def _encode(cls, lines, fo):
    """ 将一些行编码成jar可以识别的格式，写到一个临时文件中
    """
    fout = open(fo, 'w')
    for line in Encoder.encode(lines):
      if line:
        fout.write(line.encode('utf-8') + '\n')


  @classmethod
  def _decode(cls, fi):
    """ 从一个文件中读取jar的运算结果，处理成count[\t]string形式
    """
    for line in Encoder.decode(open(fi)):
      if line:
        yield line


  @classmethod
  def _mapper(cls, lines, minsup=50, algorithm="BIDE+_with_strings"):
    """ 对a list of lines 进行序列提取，将结果按行读取出来返回  
    @Parameters
    -------------------------
    | lines: iterator of unicode text
    | minsup: minimal support
    | algorithm: refer to the library spmf for a whole list of algorithms
    @Returns
    -------------------------
    | iterator of unicode lines of count\tword
    """
    ratio = str(1.0*minsup/len(lines))
    try:
      cls._encode(lines, cls.TMP_IN)
      result = subprocess.check_output(['java', '-jar', cls.JAR_PATH, 'run', algorithm, cls.TMP_IN, cls.TMP_OUT, ratio], timeout=180)
      logging.info("RUNNING: %s %s %s %s", algorithm, cls.TMP_IN, cls.TMP_OUT, ratio)
      logging.info("\n%s", result)
      for line in cls._decode(cls.TMP_OUT):
        yield line
    except:
      logging.exception("%s", len(lines))
  

  @classmethod
  def _reducer(cls, lines, counter={}):
    """ 读取iterable of 'cnt\tword', 将结果追加到一个counter中，返回counter
    @Parameters
    -------------------------
    | lines: iterator of unicode text
    | counter: accumulator of word count
    @Returns
    -------------------------
    | updated counter
    """
    for line in lines:
      try:
        fields = line.split('\t')
        cnt = int(fields[0]) 
        word = fields[1]
        if word in counter:
          counter[word] += cnt
        else:
          counter[word] = cnt
      except:
        logging.exception("%s", line)
      
    return counter


  @classmethod
  def mine_sequence(cls, lines, by=500, minsup=30):
    """ 接收一个粗排序好的文件/行列表，分成小文件，将小文件送到jar中提取序列
    然后从缓存的结果中读取出来词频，最后整合到一起
    @Parameters
    ------------------------
    | lines: iterator of unicode text
    | by: size of lines to be processed by a single mapper, larger size requires large memory and time 
    | minsup: minimal support
    @Returns
    ------------------------
    | {word: count}
    """
    counter = {}
    for group in cls._group(lines, by=by):
      res = cls._mapper(group, minsup=minsup)
      counter = cls._reducer(res, counter)
    return counter


  @staticmethod
  def _retouch(counter, minsup=50):
    """ 对一个粗提取出来的关键词进行过滤
    @Parameters
    ------------------------
    | counter: {word: count}
    | minsup: minimal support of a keyword
    @Returns
    ------------------------
    | dict
    """
    di = {}
    for word, cnt in counter.iteritems():
      if len(word) < 3:
        continue
      if cnt < minsup:
        continue
      elem = [w for w in jieba.cut(word)]
      single_word_ratio = 1.0 * len([w for w in elem if len(w) == 1])/ len(elem)
      if single_word_ratio > 0.5:
        logging.info("FILTER OUT SINGLE WORD\t%s", word)
        continue
      di[word] = cnt

    return di


  @classmethod
  def main(cls, fi, fo, minsup=50, sort=True):
    """ 程序的主入口
    @Parameters
    -------------------------
    | fi: name of file which contains lines of text
    | fo: output dict of the format count\tword
    | minsup: minimal support of a keyword
    | sort: sort the file to start, disable this option only when the file is already sorted
    """
    if sort:
      lines = Sorting.sort(fi)
    else:
      lines = (line.decode('utf-8').rstrip('\r\n') for line in open(fi))
    counter = cls.mine_sequence(lines, by=300, minsup=max(0, minsup-10)) # loose a bit for minimal support
    counter = cls._retouch(counter)
    fout = open(fo, 'w')
    for word in sorted(counter, key=counter.get, reverse=True):
      cnt = counter[word] 
      cout = unicode(cnt) + '\t' + word
      fout.write(cout.encode('utf-8')+ '\n')



if __name__ == "__main__":
  import sys
  fi = sys.argv[1]
  fo = sys.argv[2]
  try:
    minsup = int(sys.argv[3])
  except:
    minsup = 50
  Pattern.main(fi, fo, minsup, sort=True)
