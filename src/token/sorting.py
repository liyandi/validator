# -*- coding: utf-8 -*-
#!/usr/bin/python                        
##################################################
# AUTHOR : Yandi LI
# CREATED_AT : 2015-12-17
# LAST_MODIFIED : 2016-03-20 23:27:21
# USAGE : python -m token.sorting ../data/20151208 ../data/20151208_group
# PURPOSE : Sort roughly a stream of text by their similarity
##################################################
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
LOG_DIR = os.path.join(BASE_DIR, 'log')
SRC_DIR = os.path.join(BASE_DIR, 'src')
os.sys.path.insert(0, SRC_DIR) 

from hashlib import sha1
from datasketch import MinHash, LSH
from misc import mylogging 
from collections import namedtuple
import re
import string

logging = mylogging.getLogger('Sorting', LOG_DIR)

class Sorting(object):
  """
  Convert a stream of lines into a stream of groups of lines
  """
  _Line = namedtuple('Line', ['id', 'text'])
  _GROUP_INIT_THRESHOLD = 0.1
  _TOKEN_ha = re.compile('<a[^>]+>')
  _TOKEN_href = re.compile('http[s]?://[\.\w]+')
  _TOKEN_num = re.compile('[\d]+')
  _TOKEN_space = re.compile('\s+')
  _TOKEN_not_word = re.compile('\W+', re.UNICODE)
  _STOPWORDS = set(list(string.letters) + list(string.punctuation)+\
    list(ur' ，。、？～！｜■▲●…（）【】《》〈：；“”‘’★▼—\'+\"!@()*+,-./#:·;<=>?[\\]^_`{|}~⊙♥❤□︰︱﹑﹒﹕＂＃－／＜＞［］ｂｃｄｅｆｇｈｉｋｌｍｎｏｐｑｒｓｔｕｖｗｙ￣￥　')
    )

  @staticmethod
  def _get_shingles(text, size=3):
    for i in range(0, len(text)-size+1):
      yield text[i:i+size]


  @classmethod
  def _clean_text(cls, text):
    """ Basic clean the text for sketches
    Remove parts of text that shouldn't be taken as fingerprints
    @Parameters
    --------------------------------
    | text: string
    """
    text = cls._TOKEN_ha.sub('', text)
    text = cls._TOKEN_href.sub('', text)
    text = cls._TOKEN_num.sub('', text)
    text = text.replace('&nbsp;', '').replace('&lt;', '').replace('&gt;', '').replace('&amp;','')
    text = cls._TOKEN_not_word.sub(' ', text)
    text = cls._TOKEN_space.sub(' ', text)
    text = text.strip()
    return text


  @classmethod
  def _line2Line(cls, line, id=''):
    """Get input from a line in a file"""
    try:
      line = line.rstrip('\r\n').decode('utf-8')
      line = cls._clean_text(line)
      if line:
        return cls._Line(id, line)
      else:
        return None
    except:
      logging.exception('')
      return None


  @classmethod
  def _readlines(cls, fi):
    for i, line in enumerate(fi):
      line = cls._line2Line(line, i)
      if line:
        yield line


  @classmethod
  def _hash(cls, doc):
    """ Get the min hash mappings of a document
    Each document gets num_perm hashes as its profile, 
    .hashvalues() of which shows its hash values
    """
    mh = MinHash(num_perm=512)
    for shg in cls._get_shingles(doc):
      mh.digest(sha1(shg.encode('utf8')))
    return mh
    

  @staticmethod
  def _read_lines_by_group(lines, groups):
    """ read lines by groups
    @Parameters
    -----------------------------
    | lines: list
    | groups: list of same length as lines
    @Returns
    -----------------------------
    | generator of lines of the same group
    >>> for i in (T.GroupMaker._read_lines_by_group([1,2,3,4], [1,0,1,0])): print i
    ([2, 4], 0)
    ([1, 3], 1)
    >>> for i in (T.GroupMaker._read_lines_by_group([], [])): print i
    
    """
    # assert(len(lines) == len(groups))
    elems = []
    gid_old = None
    # 按照groupId对lines排序
    for gid, line in sorted(zip(groups, lines)):
      if gid_old is None or gid == gid_old:
        elems.append(line)
      else:
        yield elems #, gid_old
        elems = [line]
      gid_old = gid
    if elems:
      yield elems #, gid_old


  @classmethod
  def _initial_group(cls, lines):
    """Create an LSH index 
    that accepts MinHash objects with 512 permutations functions
    @Parameters
    -----------------------------
    | lines: list of article attributes, as a _Line() object
    @Returns
    -----------------------------
    | generator of group/list of articles
    """
    lsh = LSH(threshold=cls._GROUP_INIT_THRESHOLD, num_perm=512)  # LSH index
    hashedLines = [] # minhashes of lines
    lines = list(lines)

    ################################
    ## 将所有文章在LSH中注册
    ################################
    for line in lines:
      minh = cls._hash(line.text)
      lsh.insert(line.id, minh) # register to LSH index
      hashedLines.append(minh) 

    ################################
    ## 查询所有文章在LSH中的相似项
    ################################
    groups = [] # list of group_ids of all lines
    for hline in hashedLines:
      neighbors = lsh.query(hline) # 相似项
      gid = sorted(neighbors)[0] # 用第一个遇到的行id当做group_id
      groups.append(gid)

    for group in cls._read_lines_by_group(lines, groups):
      yield group


  @classmethod
  def _printlines(cls, groups):
    """ Print a list of Lines to files
    Consume what come from cls.groups()
    @Parameters
    ---------------------------
    | groups: iterable of lines
    """
    for group in groups:
      if not group: 
        continue
      for line in group:
        if not line:
          continue
        cout = line.text
        yield cout
   

  @classmethod
  def groups(cls, lines):
    """ Interface function of the class
    @Parameters
    ---------------------------
    | lines: list/generator of lines directly read from a file
    @Returns
    ---------------------------
    | a generator of lines as a group
    """
    lines = cls._readlines(lines)
    groups = cls._initial_group(lines)
    return groups


  @classmethod
  def test(cls, fi, fo):
    """ test main function of the class
    """
    fout = open(fo, 'w')
    groups = cls.groups(open(fi, 'r'))
    for line in cls._printlines(groups):
      fout.write(line.encode('utf-8') + '\n')


  @classmethod
  def sort(cls, fi):
    """ Main interface function of the class
    """
    groups = cls.groups(open(fi, 'r'))
    for line in cls._printlines(groups):
      yield line


if __name__ == "__main__":
  import sys
  fi = sys.argv[1]
  fo = sys.argv[2]
  Sorting.test(fi, fo)

