# -*- coding: utf-8 -*-
#!/usr/bin/python                        
##################################################
# AUTHOR : Yandi LI
# CREATED_AT : 2016-03-17
# LAST_MODIFIED : 2016-03-20 13:20:17
# USAGE : python encode.py
# PURPOSE : TODO
# java -jar spmf.jar run BIDE+_with_strings ../data/stage2/sample_ab ../data/stage3/sample_ab 1%
##################################################
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
LOG_DIR = os.path.join(BASE_DIR, 'log')
SRC_DIR = os.path.join(BASE_DIR, 'src')
os.sys.path.insert(0, SRC_DIR) 

from misc import mylogging 
import jieba

logging = mylogging.getLogger('Encoder', LOG_DIR)

class Encoder(object):

  @classmethod
  def encode(cls, lines):
    for line in lines:
      if not isinstance(line, unicode):
        line = line.decode('utf-8')
      line = line.rstrip('\r\n')
      if not line:
        continue
      nline = ' -1 '.join(w for w in jieba.cut(line) if w.strip()) + ' -2'
      yield nline


  @classmethod
  def decode(cls, lines):
    for line in lines:
      try:
        if not isinstance(line, unicode):
          line = line.decode('utf-8')
        line = line.rstrip('\r\n')
        if not line:
          continue
        seq, cnt = line.rsplit(" #SUP: ", 1)
        token = ''.join(seq.split(' -1 '))
        nline = cnt + '\t' + token
        yield nline
      except:
        pass


  @classmethod
  def main(cls, fi, fo, method='encode'):
    fout = open(fo, 'w')
    if method == 'encode':
      for line in cls.encode(open(fi)):
        fout.write(line.encode('utf-8') + '\n')
    else:
      for line in cls.decode(open(fi)):
        fout.write(line.encode('utf-8') + '\n')
      

if __name__ == "__main__":
  import sys
  if len(sys.argv) > 3:
    method = sys.argv[1]
    fi = sys.argv[2]
    fo = sys.argv[3]
  else:
    method = 'encode'
    fi = sys.argv[1]
    fo = sys.argv[2]

  Encoder.main(fi, fo, method)
