Validator
===============================
Intended to provide a tool to identify main readable text part on a webpage.

This work is done primarily for creating a universal framework for my recent project, 
called [Weibo Toutiao](https://play.google.com/store/apps/details?id=com.sina.app.weiboheadline), 
a news provider that collects data shared by users on weibo.com. 

---
To install, simply do `python setup.py install --user`
Then create essential folders and download necessary libraries using `bash config.sh` 
    
Then to use the module:

    >>> import validator

---

#### 1. 词典挖掘器 Token extractor
读入一段网页中的文本，提取其中重复出现的文字序列。提出的文本可以作为备选的结构性文本关键词的词典。
Scan over a corpus of webpage paragrahps, discover recurrent text sequences as potential extraneous elements.

The sequence mining algorithm is imported from an open-source Java package [SPMF](http://www.philippe-fournier-viger.com/spmf/index.php?link=algorithms.php). 

To train a list of keywords,

    $ cd src/
    $ python -m keywords.pattern <PATH_TO_TEXT_FILE> <PATH_TO_OUTPUT_DICT> [OPTIONAL:mininal support]

**Note**

  - A normal size of the input corpus is fewer than 150,000 lines.
  - In some cases, the java library throws a out-of-memory exception, normally you can savely ignore this error.
  - By default, the resulting dictionary consists of recurrent patterns of minimal support 50.

To be continued...