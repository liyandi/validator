#!/bin/bash

############################
## CREATE FOLDERS
############################
BASE_DIR="$(pwd)"
mkdir -pv $BASE_DIR/log
mkdir -pv $BASE_DIR/data/
mkdir -pv $BASE_DIR/data/tmp/
mkdir -pv $BASE_DIR/lib/

echo "ALL FOLDERS CREATED"


############################
## DOWNLOADING LIBRARIES
############################
wget "http://www.philippe-fournier-viger.com/spmf/spmf.jar" -O $BASE_DIR/lib/spmf.jar
echo "LIBRARIES DOWNLOADED"


