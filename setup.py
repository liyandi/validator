# -*- coding: utf-8 -*-
#!/usr/bin/python                        
##################################################
# AUTHOR : Yandi LI
# CREATED_AT : 2016-03-13
# LAST_MODIFIED : 2016-03-20 16:02:27
# USAGE : python setup.py
# PURPOSE : TODO
##################################################
from __future__ import absolute_import
from setuptools import setup, find_packages

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='validator',
      version='0.1',
      description='Tools that detect elements out of the scope of readable content',
      long_description=readme(),
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.6',
        'Topic :: Text Processing :: Linguistic',
      ],
      keywords='webpage crawling',
      url='',
      author='Yandi LI',
      author_email='yandi@staff.weibo.com',
      license='MIT',
      packages = find_packages('.'),
      package_data = {}, 
      install_requires=[
          "subprocess32", "jieba", "datasketch"
      ],
      include_package_data=False,
      zip_safe=False)


